
console.log("Bài 1");
/* ----------Bài 1 ----------
* Đầu vào: Lương 1 ngày: 100.000, số ngày làm
*
* Các bước xử lý:
* B1: Tạo 1 biến lương 1 ngày và gán giá trị.
* B2: Tạo 1 biến số ngày làm và gán giá trị.
* B3: Tạo 1 biến tính lương.
* B4: Sử dụng công thức tính lương: Lương 1 ngày x số ngày làm.
* B5: In kết quả ra console.
*
* Đầu ra: Tiền lương của nhân viên.
*/

var luong_mot_ngay = 100000;
var   so_ngay_lam = 30;
var   tinh_luong;

tinh_luong = luong_mot_ngay*so_ngay_lam;

console.log("🚀 ~ file: index.js ~ line 23 ~ tinh_luong", tinh_luong)

/* ----------Bài 2 ----------

* Đầu vào: 5 số thực
*
* Các bước xử lý:
* B1: Tạo 5 biến và gán giá trị số thực cho mỗi biến.
* B2: Tạo 1 biến trung bình.
* B3: Sử dụng công thức tính trung bình: Tổng 5 số thực / 5.
* B4: In kết quả ra console.
*
* Đầu ra: Giá trị trung bình của 5 số.
*/
console.log("Bài 2");
var num1 = 5.60;
var num2 = 4.53;
var num3 = 8.83;
var num4 = 9.18;
var num5 = 7.53;

var trung_binh;

trung_binh = (num1+num2+num3+num4+num5)/5;

console.log("🚀 ~ file: index.js ~ line 48 ~ trung_binh", trung_binh)



/* ----------Bài 3 ----------

* Đầu vào: tỷ giá USD, số tiền USD
*
* Các bước xử lý:
* B1: Tạo 1 biến tỷ giá USD và gán giá trị.
* B2: Tạo 1 biến số tiền USD và gán giá trị.
* B3: Tạo 1 biến quy đổi.
* B4: Sử dụng công thức quy đổi: tỷ giá USD * số tiền USD .
* B5: In kết quả ra console.
*
* Đầu ra: Giá trị USD ra VNĐ.
*/
console.log("Bài 3");
var ty_gia_USD = 23500;
var so_tien_USD = 2;

var quy_doi;

quy_doi = ty_gia_USD*so_tien_USD;

console.log("🚀 ~ file: index.js ~ line 71 ~ quy_doi", quy_doi)

/* ----------Bài 4 ----------

* Đầu vào: chiều dài và chiều rộng hình chữ nhật (HCN).
*
* Các bước xử lý:
* B1: Tạo 2 biến tương ứng chiều dài và chiều rộng và gán giá trị.
* B2: Tạo 1 biến diện tích.
* B3: Sử dụng công thức tính diện tích: dài * rộng .
* B4: Tạo 1 biến chu vi.
* B5: Sử dụng công thức tính chu vi: (dài + rộng)x2.
* B6: In kết quả ra console.
*
* Đầu ra: Diện tích và chu vi HCN.
*/
console.log("Bài 4");
var dai = 5;
var rong = 2;

var dien_tich;
dien_tich = dai*rong;

var chu_vi;
chu_vi = (dai+rong)*2;

console.log("🚀 ~ file: index.js ~ line 94 ~ dien_tich", dien_tich)

console.log("🚀 ~ file: index.js ~ line 97 ~ chu_vi", chu_vi)

/* ----------Bài 5 ----------

* Đầu vào: 1 số có 2 chữ số.
*
* Các bước xử lý:
* B1: Tạo 1 biến và gán giá trị 1 số có 2 chữ số.
* B2: Tạo 1 biến hàng chục.
* B3: Sử dụng công thức lấy hàng chục: so/10.
* B4: Tạo 1 biến hàng đơn vị.
* B5: Sử dụng công thức lấy hàng đơn vị: so%10.
* B6: Tạo 1 biến tổng.
* B7: Sử dụng công thức tính tổng: chục + đơn vị.
* B8: In kết quả ra console.
*
* Đầu ra: tổng 2 ký số.
*/
console.log("Bài 5");
var so = 39;

var chuc;
chuc = Math.floor(so/10);

var donvi;
donvi = so%10;

var tong;
tong = chuc+donvi;

console.log("🚀 ~ file: index.js ~ line 129 ~ tong", tong)
